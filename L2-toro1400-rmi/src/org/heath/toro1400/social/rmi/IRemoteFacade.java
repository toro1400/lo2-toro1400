package org.heath.toro1400.social.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;

public interface IRemoteFacade extends Remote {
	public void addEventGraph(Set<User> users, Set<Event> events, Set<Comment> comments) throws RemoteException;
}
