package org.heath.toro1400.social.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.Set;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;
import org.heath.toro1400.social.rmi.IRemoteFacade;

public class Client implements IRemoteFacade {
	private IRemoteFacade remoteFacade;
	
	public Client(Registry registry) {
		try {
			remoteFacade = (IRemoteFacade) registry.lookup("jee16");
		} catch (NotBoundException | RemoteException e) {
			throw new RuntimeException("Could not bind Interface");
		}
	}
	
	public void addEventGraph(Set<User> users, Set<Event> events, Set<Comment> comments) throws RemoteException {
		remoteFacade.addEventGraph(users, events, comments);
	}

}
