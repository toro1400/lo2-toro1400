package org.heath.toro1400.social.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.heath.toro1400.social.parser.Parser;

import jdk.nashorn.internal.runtime.ParserException;

public class DatabasePopulator {
	
	private static final String defaultUrl = "https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt";
	private final static int registryPort = 1100;
	private final Client client;
	private final String urlString;
	
	public DatabasePopulator(String url, Registry registry) {
		client = new Client(registry);
		this.urlString = url;
	}
	
	public static void main(String[] args) throws RemoteException {
		DatabasePopulator dp = null;
		
		switch (args.length) {
			case 0:
				dp = new DatabasePopulator(defaultUrl, LocateRegistry.getRegistry(registryPort));
				break;
			case 1:
				dp = new DatabasePopulator(args[0], LocateRegistry.getRegistry(registryPort));
				break;
			case 2:
				dp = new DatabasePopulator(args[0], LocateRegistry.getRegistry(args[1], registryPort));
				break;
		}
		
		dp.parse();
	}
			
	public void parse() {
		InputStream is = null;
		Parser parser;		
		
		try {
			URL url = new URL(urlString);
			is = url.openStream();
			parser = new Parser(is);
			parser.parse();
		
			client.addEventGraph(parser.getUsers(), parser.getEvents(), parser.getComments());
			
		} catch (IOException | ParserException e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {}
		}
	}
}
