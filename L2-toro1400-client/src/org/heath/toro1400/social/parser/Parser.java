package org.heath.toro1400.social.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;



public class Parser {

	private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
	
	private BufferedReader bufferedReader;
	private Set<User> users;
	private Set<Event> events;
	private Set<Comment> comments;
	
	public Parser(InputStream is) {
		this.bufferedReader = new BufferedReader(new InputStreamReader(is));
		users = new HashSet<User>();
		events = new HashSet<Event>();
		comments = new HashSet<Comment>();
	}
	
	public Set<User> getUsers() {
		return users;
	}
	
	public Set<Event> getEvents() {
		return events;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}
	
	public void parse() {
		try {
			String line = bufferedReader.readLine();
			while (!line.equals("") && line != null) {
				if (!line.equals("Users:")) {
					String[] userData = line.split("\\s+", 3);
					users.add(new User(userData[0], userData[1], userData[2]));
				}
				line = bufferedReader.readLine();
			}
			line = bufferedReader.readLine();
			// Parse events
			while (!line.equals("") && line != null) {
				if (!line.equals("Events:")) {
					Pattern p = Pattern.compile("([^,]+)(?:, )([^,]+)(?:, )([^,]+)(?:, )((?:[^ ]+)(?: )(?:[^ ]+))(?: - )((?:[^ ]+)(?: )(?:[^,]+))");
					Matcher m = p.matcher(line);
					m.find();
					
					String title = m.group(1);
					String city = m.group(2);
					String content = m.group(3);
					LocalDateTime startTime = LocalDateTime.parse(m.group(4), dateTimeFormatter);
					LocalDateTime stopTime = LocalDateTime.parse(m.group(5), dateTimeFormatter);
					
					p = Pattern.compile("(?:\\[)((?:[^\\]])+)");
					m = p.matcher(line);
					m.find();
					
					String emailAddresses[] = m.group(1).split(", ");
					Set<User> organizers = new HashSet<User>();
					for (String email : emailAddresses) {
						for (User user : users) {
							if (user.getEmail().equals(email)) {
								organizers.add(user);
								break;
							}
						}
					}
					
					Event event = new Event(title, city, content, startTime, stopTime, organizers);
					
					line = bufferedReader.readLine();
					while (!line.equals("") && line != null) {
						p = Pattern.compile("\\t([^ ]+) \\(((?:[\\d]{2}[\\/ :]?){5})\\)\\: \\\"([^\"]*)\\\"");
						m = p.matcher(line);
						m.find();
						
						User author = null;
						for (User user : users) {
							if (user.getEmail().equals(m.group(1))) {
								author = user;
								break;
							}
						}
						
						comments.add(new Comment(event, author, m.group(3), LocalDateTime.parse(m.group(2), dateTimeFormatter)));
						line = bufferedReader.readLine();
					}
					events.add(event);
				}
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			// Parsing done!
		}
		
	}
}
