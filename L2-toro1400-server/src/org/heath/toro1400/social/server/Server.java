package org.heath.toro1400.social.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.heath.toro1400.social.rmi.IRemoteFacade;

public class Server {
	private final static int registryPort = 1100;
	private RemoteFacade facade;
	private SocialDAO dao;
	
	public Server(EntityManagerFactory emf) {
		dao = new SocialDAO(emf);
		facade = new RemoteFacade(dao);
	}
	
	public void start() {
		try {
			Object facadeInterface = UnicastRemoteObject.exportObject((Remote)facade, 0);
			Registry registry = null;
			try {
				registry = LocateRegistry.getRegistry(registryPort);
				registry.list();
			} catch (RemoteException e) {
				registry = LocateRegistry.createRegistry(registryPort);
			}
			registry.rebind("jee16", (IRemoteFacade) facadeInterface);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Server server = new Server(Persistence.createEntityManagerFactory("social"));
		server.start();
	}
}
