package org.heath.toro1400.social.server;

import java.rmi.RemoteException;
import java.util.Set;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;
import org.heath.toro1400.social.rmi.IRemoteFacade;


public class RemoteFacade implements IRemoteFacade {

	private SocialDAO dao;
	
	public RemoteFacade(SocialDAO dao) {
		this.dao = dao;
	}
	
	public void addEventGraph(Set<User> users, Set<Event> events, Set<Comment> comments) throws RemoteException {
		dao.persistAll(users, events, comments);
	}

}
